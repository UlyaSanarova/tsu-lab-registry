#ifndef CONSULTING_COMPANY_H
#define CONSULTING_COMPANY_H

#include "company.h"

// Класс консалтингового предприятия
class ConsultingCompany : public Company {
public:
    ConsultingCompany();

    Company::Type getType() const override;

    double getTax() const override;
};

#endif // CONSULTING_COMPANY_H
