#ifndef COMPANY_H
#define COMPANY_H

#include <QString>
#include <QList>

// Класс "Предприятие"
class Company
{
public:
    // Конструктор по умолчанию
    Company();

    // Виртуальный деструктор, т.к. класс имеет абстрактные методы
    virtual ~Company();

    // Перечисление типов компаний
    enum Type {
        // Строительная
        TYPE_Building,
        // IT
        TYPE_IT,
        // Косалтинговая
        TYPE_Consulting,
    };

    // Получить имя компании
    const QString& getName() const;

    // Установить имя компании
    void setName(const QString& name);

    // Получить список владельцев компании
    const QList<QString>& getOwners() const;

    // Установить список владельцев компании
    void setOwners(const QList<QString>& owners);

    // Получить доход компании
    double getIncome() const;

    // Установить доход компании
    void setIncome(double income);

    // Получить площадь компании
    double getSquare() const;

    // Установить площадь компании
    void setSquare(double square);

    // Получить кол-во сотрудников
    int getNumberOfEmployees() const;

    // Установить кол-во сотрудников
    void setNumberOfEmployees(int numberOfEmployees);

    // Получить тип компании
    virtual Type getType() const = 0;

    // Получить налог
    virtual double getTax() const = 0;

private:
    // Имя компнии
    QString name_;
    // Список владельцев компании
    QList<QString> owners_;
    // Доход компании
    double income_;
    // Площадь компании
    double square_;
    // Кол-во сотрудников
    int numberOfEmployees_;
};

#endif // COMPANY_H
