#include "company.h"

Company::Company() {
    income_ = 0;
    square_ = 0;
    numberOfEmployees_ = 0;
}

Company::~Company() {}

const QString& Company::getName() const {
    return name_;
}

void Company::setName(const QString& name) {
    name_ = name;
}

const QList<QString>& Company::getOwners() const {
    return owners_;
}

void Company::setOwners(const QList<QString>& owners) {
    owners_ = owners;
}

double Company::getIncome() const {
    return income_;
}

void Company::setIncome(double income) {
    income_ = income;
}

double Company::getSquare() const {
    return square_;
}

void Company::setSquare(double square) {
    square_ = square;
}

int Company::getNumberOfEmployees() const {
    return numberOfEmployees_;
}

void Company::setNumberOfEmployees(int numberOfEmployees) {
    numberOfEmployees_ = numberOfEmployees;
}
