#include <iostream>
#include <map>
#include "building_company.h"
#include "it_company.h"
#include "consulting_company.h"
#include "registry.h"

// Функция для печати типа компании
void printCompanyType(Company::Type type) {
    using namespace std;
    switch (type) {
        case Company::TYPE_Building:
            cout << "Building" << endl;
            break;
        case Company::TYPE_IT:
            cout << "IT" << endl;
            break;
        case Company::TYPE_Consulting:
            cout << "Consulting" << endl;
            break;
        default:
            cout << "<unknown>" << endl;
            break;
    }
}

// Функция для вывода информации о предприятии
void printCompanyInfo(Company* c) {
    using namespace std;
    if (!c) return;
    cout << "Company \"" << c->getName().toStdString() << "\"" << endl;
    cout << "       Type: ";
    printCompanyType(c->getType());
    cout << "     Owners: ";
    QList<QString> owners = c->getOwners();
    if (owners.empty()) {
        cout << "<nobody>" << endl;
    } else {
        cout << owners.join(", ").toStdString() << endl;
    }
    cout << "     Square: " << c->getSquare() << endl;
    cout << "     Income: " << c->getIncome() << endl;
    cout << "  Employees: " << c->getNumberOfEmployees() << endl;
    cout << "        Tax: " << c->getTax() << endl;
}

// Функция для вывода компаний по их типу
void printCompaniesByType(Company::Type type) {
    // получаем реестр
    Registry* r = Registry::getInstance();
    if (!r) return;

    // получаем кол-во компаний
    int num = r->getNumberOfCompanies();
    for (int i = 0; i < num; i++) {
        // получаем компанию
        Company* c = r->get(i);
        // если предприятие существует и его тип соответствует трубуемуму,
        //   то выводим его
        if (c && c->getType() == type) {
            printCompanyInfo(c);
        }
    }
}

// Функция для вывода компаний по их владельцу
void printCompaniesByOwner(const QString& owner) {
    // получаем реестр
    Registry* r = Registry::getInstance();
    if (!r) return;

    // получаем кол-во компаний
    int num = r->getNumberOfCompanies();
    for (int i = 0; i < num; i++) {
        // получаем компанию
        Company* c = r->get(i);
        // если предприятие существует и список ого владельцев содержит
        //   заданного владельца, то выводим его
        if (c && c->getOwners().contains(owner)) {
            printCompanyInfo(c);
        }
    }
}

// Функция для печати средних значений по каждому типу компаний
void printAverageIncomeSquareEmployees() {
    using namespace std;

    // получаем реестр
    Registry* r = Registry::getInstance();
    if (!r) return;

    // карты для дохода и площади
    map<Company::Type, double> income, square;
    // карты для кол-ва сотрудников и кол-ва компаний
    map<Company::Type, int> employees, size;

    // получаем кол-во компаний
    int num = r->getNumberOfCompanies();
    for (int i = 0; i < num; i++) {
        // получаем компанию
        Company* c = r->get(i);
        if (c) {
            // получаем тип предприятия
            Company::Type type = c->getType();
            // суммируем требуемые значения
            income[type] += c->getIncome();
            square[type] += c->getSquare();
            employees[type] += c->getNumberOfEmployees();
            size[type]++;
        }
    }

    // составляем список типов предприятий
    QList<Company::Type> companyTypes;
    companyTypes << Company::TYPE_Building << Company::TYPE_IT
                 << Company::TYPE_Consulting;

    // проходимся по этому списку
    for (Company::Type type : companyTypes) {
        // получаем кол-во предприятий
        int s = size[type];
        // если кол-во > 0, то начинаем расчеты
        if (s != 0) {
            // считаем
            double in = income[type] / s;
            double sq = square[type] / s;
            double em = double(employees[type]) / s;
            // и выводим
            cout << "Company Type: ";
            printCompanyType(type);
            cout << "     Avg. Income: " << in << endl;
            cout << "     Avg. Square: " << sq << endl;
            cout << "  Avg. Employees: " << em << endl;
        }
    }
}

int main() {
    ITCompany testCompany;
    testCompany.setName("Test");
    testCompany.setOwners(QStringList() << "Owner 1" << "Owner 2");
    testCompany.setSquare(1000);
    testCompany.setIncome(1000000);
    testCompany.setNumberOfEmployees(100);

    ITCompany testCompany2;
    testCompany2.setName("Test 2");
    testCompany2.setOwners(QStringList() << "Owner 1");
    testCompany2.setSquare(10000);
    testCompany2.setIncome(10000000);
    testCompany2.setNumberOfEmployees(1000);

    BuildingCompany testCompany3;
    testCompany3.setName("Test 3");
    testCompany3.setOwners(QStringList() << "Owner 2");
    testCompany3.setSquare(10000);
    testCompany3.setIncome(10000000);
    testCompany3.setNumberOfEmployees(1000);

    Registry* r = Registry::getInstance();
    r->add(&testCompany);
    r->add(&testCompany2);
    r->add(&testCompany3);

    std::cout << "printAverageIncomeSquareEmployees()" << std::endl;
    printAverageIncomeSquareEmployees();
    std::cout << std::endl;

    std::cout << "printCompaniesByOwner(\"Owner 2\")" << std::endl;
    printCompaniesByOwner("Owner 2");
    std::cout << std::endl;

    std::cout << "printCompaniesByType(Company::TYPE_IT)" << std::endl;
    printCompaniesByType(Company::TYPE_IT);
    std::cout << std::endl;

    return 0;
}
