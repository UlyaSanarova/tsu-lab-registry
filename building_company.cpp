#include "building_company.h"

BuildingCompany::BuildingCompany() {}

Company::Type BuildingCompany::getType() const {
    return TYPE_Building;
}

double BuildingCompany::getTax() const {
    return getSquare() / (getOwners().size() + getNumberOfEmployees());
}
