#ifndef IT_COMPANY_H
#define IT_COMPANY_H

#include "company.h"

// Класс IT-предприятия
class ITCompany : public Company {
public:
    ITCompany();

    Company::Type getType() const override;

    double getTax() const override;
};

#endif // IT_COMPANY_H
