#ifndef REGISTRY_H
#define REGISTRY_H

#include <QList>
#include "company.h"

// Класс-Singleton "Реестр"
class Registry {
    // Конструктор по-умолчанию
    Registry();

    // Деструктор
    ~Registry();

public:
    // Получить экземпляр реестра
    static Registry* getInstance();

    // Добавить предприятие в реестр
    void add(Company* c);

    // Удалить предприятие из реестра
    void remove(Company* c);

    // Получить кол-во зарегистрированных предприятий
    int getNumberOfCompanies() const;

    // Получиь предприятие по индексу
    Company* get(int i) const;

private:
    // Список зарегистрированных предприятий
    QList<Company*> companies_;
};

#endif // REGISTRY_H
