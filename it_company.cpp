#include "it_company.h"

ITCompany::ITCompany() {}

Company::Type ITCompany::getType() const {
    return TYPE_IT;
}

double ITCompany::getTax() const {
    return getIncome() / (getOwners().size() + getNumberOfEmployees());
}
