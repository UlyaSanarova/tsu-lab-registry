#ifndef BUILDING_COMPANY_H
#define BUILDING_COMPANY_H

#include "company.h"

// Класс строительного предприятия
class BuildingCompany : public Company {
public:
    BuildingCompany();

    Type getType() const override;

    double getTax() const override;
};

#endif // BUILDING_COMPANY_H
