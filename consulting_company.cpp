#include "consulting_company.h"

ConsultingCompany::ConsultingCompany() {}

Company::Type ConsultingCompany::getType() const {
    return TYPE_Consulting;
}

double ConsultingCompany::getTax() const {
    return getIncome() / getNumberOfEmployees();
}
