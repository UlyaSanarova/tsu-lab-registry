#include "registry.h"

Registry::Registry() {}

Registry::~Registry() {}

Registry* Registry::getInstance() {
    // созаем статический объект
    static Registry r;
    // и возвращаем его адрес
    return &r;
}

void Registry::add(Company* c) {
    if (c && !companies_.contains(c)) {
        // добавляем предприятие, если оно существует и ещё не добалено в реестр
        companies_.append(c);
    }
}

void Registry::remove(Company* c) {
    if (c && companies_.contains(c)) {
        // удаляем предприятие, если оно существует и уже добалено в реестр
        companies_.removeOne(c);
    }
}

int Registry::getNumberOfCompanies() const {
    return companies_.size();
}

Company* Registry::get(int i) const {
    // если индекс не валидный, то возвращаем нулевой указатель
    if (i < 0 || i >= companies_.size())
        return nullptr;
    return companies_.at(i);
}
